
const request = fetch("https://ajax.test-danit.com/api/swapi/films");

request.then((result) => result.json())
.then((data) => {
  data.forEach(element => {

    const div = document.createElement("div");
    document.body.appendChild(div);
    
    const id = document.createElement("p")
    id.textContent = element.episodeId;
    div.append(id);
    
    const name = document.createElement("span")
    name.setAttribute('id', 'name')
    name.textContent = element.name;
    div.append(name);

    const charactersList = element.characters
    for (key of charactersList) {
      const url = fetch(key);
      url.then((result) => result.json())
      .then((data) => {
        const characterName = document.createElement('p');
        characterName.textContent = data.name;
        div.children[2].before(characterName);
      })
    }
    
    const crawl = document.createElement("p");    
    crawl.textContent = element.openingCrawl;
    div.append(crawl)
  });
})
